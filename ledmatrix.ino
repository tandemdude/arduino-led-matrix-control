int pinArray[8] = {A3, A2, A1, A0, 13, 12, 11, 10};
int alwaysOnPinArray[8] = {2, 3, 4, 5, 6, 7, 8, 9};

int maxFrames = 7;

int previousRowPointer = 7;
int rowPointer = 0;

byte blank_space[8] = {B00000000, B00000000, B00000000, B00000000, B00000000, B00000000, B00000000, B00000000};
byte letter_o[8] = {B11111110, B10000010, B10000010, B10000010, B10000010, B10000010, B11111110, B00000000};
byte letter_l[8] = {B10000000, B10000000, B10000000, B10000000, B10000000, B10000000, B11111110, B00000000};
byte letter_i[8] = {B11111110, B00010000, B00010000, B00010000, B00010000, B00010000, B11111110, B00000000};
byte letter_v[8] = {B10000010, B10000010, B01000100, B01000100, B00101000, B00101000, B00010000, B00000000};
byte letter_e[8] = {B11111110, B10000000, B10000000, B11111000, B10000000, B10000000, B11111110, B00000000};
byte letter_r[8] = {B11111110, B10000010, B10000010, B11111100, B10000010, B10000010, B10000010, B00000000};

byte* displayOrder[] = {blank_space, letter_o, letter_l, letter_i, letter_v, letter_e, letter_r, blank_space};
//byte* displayOrder[] = {letter_v, blank_space, blank_space};

byte displayBuffer[8] = {
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000
};

void incrementRowPointer() {
  rowPointer++;
  if (rowPointer > 7) {
    rowPointer = 0;
  }
  previousRowPointer++;
  if (previousRowPointer > 7) {
    previousRowPointer = 0;
  }
}

void displayRow() {
  byte row = displayBuffer[rowPointer];
  for (int j = 0; j < 8; j++) {
    if (row & (1 << j)) {
      digitalWrite(pinArray[j], LOW);
    } else {
      digitalWrite(pinArray[j], HIGH);
    }
  }
}

void drawToMatrix() {
  digitalWrite(alwaysOnPinArray[previousRowPointer], LOW);
  displayRow();
  digitalWrite(alwaysOnPinArray[rowPointer], HIGH);
  incrementRowPointer();
  delay(1);
}

void writeToDisplayBuffer(byte rows[8]) {
  for (int i = 0; i < 7; i++) {
    displayBuffer[i] = rows[i];
  }
}

/*
void scrollDisplay() {
  int setlsb = 0;
  for (int row = 0; row < 8; row++) {
    setlsb = 0;
    for (int i = maxFrames-1; i >= 0; i--) {
      byte oldRow = displayOrder[i][row];
      displayOrder[i][row] = displayOrder[i][row] << 1;
      if (setlsb == 1) {
        displayOrder[i][row] += 1;
      }
      if (oldRow & (1 << 7)) {
        setlsb = 1;
      } else {
        setlsb = 0;
      }
    }
  }
}
*/

void scrollDisplay() {
  Serial.println("Scrolling");
  for (int row = 0; row < 8; row++) {
    for (int i = 0; i < maxFrames; i++) {
      displayOrder[i][row] = displayOrder[i][row] << 1;
      if (displayOrder[i + 1][row] & (1 << 7)) {
        displayOrder[i][row] += 1;
      }
    }
  }
}

void setup () {
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(A0, OUTPUT);
  pinMode(A1, OUTPUT);
  pinMode(A2, OUTPUT);
  pinMode(A3, OUTPUT);
  Serial.begin(9600);
  writeToDisplayBuffer(displayOrder[0]);
}

void loop () {
  int millisSinceStart = millis();
  if (!(millisSinceStart % 500) && millisSinceStart) {
    scrollDisplay();
    writeToDisplayBuffer(displayOrder[0]);
  }
  drawToMatrix();
}

